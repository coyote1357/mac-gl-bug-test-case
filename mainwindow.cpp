#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QDebug>
#include <gst/video/videooverlay.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    if (!gst_is_initialized())
    {
        gst_init(nullptr,nullptr);
    }
    m_videoSink1 = gst_element_factory_make("glimagesink", "videoSink1");
    m_videoSink2 = gst_element_factory_make("glimagesink", "videoSink2");
    m_pipeline = gst_pipeline_new("cdgPipeline");
    auto testSrc = gst_element_factory_make("videotestsrc", "testSrc");
    auto videoConvert = gst_element_factory_make("videoconvert", "cdgVideoConv");
    auto videoQueue1 = gst_element_factory_make("queue", "videoQueue1");
    auto videoQueue2 = gst_element_factory_make("queue", "videoQueue2");
    auto videoConv1 = gst_element_factory_make("videoconvert", "preOutVideoConvert1");
    auto videoConv2 = gst_element_factory_make("videoconvert", "preOutVideoConvert2");
    auto videoScale1 = gst_element_factory_make("videoscale", "videoScale1");
    auto videoScale2 = gst_element_factory_make("videoscale", "videoScale2");
    auto videoTee = gst_element_factory_make("tee", "videoTee");
    auto videoTeePad1 = gst_element_get_request_pad(videoTee, "src_%u");
    auto videoTeePad2 = gst_element_get_request_pad(videoTee, "src_%u");
    auto videoQueue1SrcPad = gst_element_get_static_pad(videoQueue1, "sink");
    auto videoQueue2SrcPad = gst_element_get_static_pad(videoQueue2, "sink");
    gst_bin_add_many(reinterpret_cast<GstBin *>(m_pipeline), testSrc, videoConvert, videoConv1,
                     videoConv2, videoTee, videoQueue1, videoQueue2, videoScale1, videoScale2,
                     m_videoSink1, m_videoSink2,nullptr);
    gst_element_link_many(testSrc, videoConvert, videoTee, nullptr);
    gst_pad_link(videoTeePad1, videoQueue1SrcPad);
    gst_pad_link(videoTeePad2, videoQueue2SrcPad);
    gst_element_link_many(videoQueue1, videoConv1, videoScale1, m_videoSink1, nullptr);
    gst_element_link_many(videoQueue2, videoConv2, videoScale2, m_videoSink2, nullptr);
    gst_video_overlay_set_window_handle(reinterpret_cast<GstVideoOverlay*>(m_videoSink1), ui->videoWidget1->winId());
    gst_video_overlay_set_window_handle(reinterpret_cast<GstVideoOverlay*>(m_videoSink2), ui->videoWidget2->winId());

    connect(ui->pushButtonPlay, &QPushButton::clicked, [&] () {
        gst_element_set_state(m_pipeline, GST_STATE_NULL);
        gst_video_overlay_set_window_handle(reinterpret_cast<GstVideoOverlay*>(m_videoSink1), ui->videoWidget1->winId());
        gst_video_overlay_set_window_handle(reinterpret_cast<GstVideoOverlay*>(m_videoSink2), ui->videoWidget2->winId());
        gst_element_set_state(m_pipeline, GST_STATE_PLAYING);
    });
    connect(ui->pushButtonStop, &QPushButton::clicked, [&] () {
        gst_element_set_state(m_pipeline, GST_STATE_NULL);
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

